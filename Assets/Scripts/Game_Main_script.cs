﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game_Main_script : MonoBehaviour
{
    public static int game_score=0;
    private int length_x = 0, length_y = 0, length_z = 0;
    public GameObject cube;
    public int countCubes_x, countCubes_z, countCubes_y;
    public GameObject background;
    private GameObject[,,] massiv_kubikov;
    private int figure_count=1;
    public float player_distance;
    private float player_x, player_y, player_z;
    private GameObject player;
    public Text end_of_game_text, game_score_text;
    private float y = 0.0f, x = 0.0f, z = 0.0f;
    public Camera main_camera;
    public GameObject floor;
    private float last_y;
    private bool animation_once = true;
    public int speed_of_animation;
    private bool first_back_click = false;
    public Text exit_info;
    private float time_remain_for_exit;

    //Пауза
    static public bool pause_status;
    public Text pause_info;
    public Button pause_button;

    bool Creation_figure(int x, int y, int z, GameObject temp, Color color)
    { 
        if (massiv_kubikov[y, z, x].gameObject.transform.parent == null)
        {
            massiv_kubikov[y, z, x].transform.parent = temp.transform;
            Destroy(massiv_kubikov[y, z, x].GetComponent<Rigidbody>());
            massiv_kubikov[y, z, x].GetComponent<Renderer>().material.color = color;
            return true;
        }
        else
        {
            return false;
        }
    }

    void Create_figure(int x, int y, int z, GameObject temp, bool start = false)
    {
        int dl;
        Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        length_x = Random.Range(0 - x, countCubes_x - x);
        length_y = Random.Range(0 - y, 3 - y);
        length_z = Random.Range(0 - z, countCubes_z - z);
        if (x + length_x >= 0)
        {
            for (dl = x; dl < x + length_x; dl++)
            {
                if (!Creation_figure(dl, y, z, temp, color))
                    break;
            }
            x = dl;
        }
        else
        {
            for (dl = x; dl > x + length_x; dl--)
            {
                if (!Creation_figure(dl, y, z, temp, color))
                    break;
            }
            x = dl;
        }
        if (y + length_y >= 0)
        {
            for (dl = y; dl < y + length_y; dl++)
            {
                if (!Creation_figure(x, dl, z, temp, color))
                    break;
            }
            y = dl;
        }
        else
        {
            for (dl = y; dl > y + length_y; dl--)
            {
                if (!Creation_figure(x, dl, z, temp, color))
                    break;
            }
            y = dl;
        }
        if (z + length_z >= 0)
        {
            for (dl = z; dl < z + length_z; dl++)
            {
                if (!Creation_figure(x, y, dl, temp, color))
                    break;
            }
        }
        else
        {
            for (dl = z; dl > z + length_z; dl--)
            {
                if (!Creation_figure(x, y, dl, temp, color))
                    break;
            }
        }
        if (massiv_kubikov[y, z, x].transform.parent == null)
        {
            Creation_figure(x, y, z, temp, color);
        }
        if (start)
        {
            temp.AddComponent<Rigidbody>();
           // temp.AddComponent<Rigidbody>().drag = 0.7f;
        }
        temp.tag = "Figure";
        figure_count++;
    }


    void Create_layer(float start_y, bool start=false)
    {
        if (!start)
        {
            try
            {
                for (int i = 0; i < GameObject.FindGameObjectsWithTag("Figure").Length; i++)
                {
                    if (!GameObject.FindGameObjectsWithTag("Figure")[i].GetComponent<Rigidbody>())
                        GameObject.FindGameObjectsWithTag("Figure")[i].AddComponent<Rigidbody>();
                   // GameObject.FindGameObjectsWithTag("Figure")[i].AddComponent<Rigidbody>().drag = 0.7f;
                }
            }
            catch
            { }
            
        }
        y = start_y;
        massiv_kubikov = new GameObject[3, countCubes_z, countCubes_x];
        /////////////////////// СОЗДАНИЕ ВСЕХ КУБИКОВ////////////////////////////////////////
        for (int u = 0; u < 3; u++)
        {
            z = 0f;
            for (int p = 0; p < countCubes_z; p++)
            {
                x = 0f;
                for (int i = 0; i < countCubes_x; i++)
                {
                    massiv_kubikov[u,p,i]= Instantiate(cube, new Vector3(x, y, z), Quaternion.identity);
                    x += cube.transform.localScale.x;
                }
                z += cube.transform.localScale.z;
            }
            y += cube.transform.localScale.y;
        }
        //////////////////////////////////////////////////////////////////////////////////////
      
        z = this.transform.localScale.z;
        //////////////////////////////////////////////////////////////// СОЗДАНИЕ ФИГУР/////////////////////////////////////////////////////////////
        for (int u = 0; u < 3; u++)
        {
            for (int p = 0; p < countCubes_z; p++)
            {
                for (int i = 0; i < countCubes_x; i++)
                {
                    if (massiv_kubikov[u,p,i].gameObject.transform.parent == null)
                    {
                            Create_figure(i, u, p, new GameObject("фигура "+figure_count.ToString()),start);
                    }
                }
            }
        }
        last_y = massiv_kubikov[0, 0, 0].transform.localPosition.y-3.0f;
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    void Create_player()
    {
        //////////////////////////////////////////// СОЗДАНИЕ ШАРИКА/////////////////////////////////////////////////////////////////////////////////////
        player = GameObject.FindGameObjectWithTag("Player");
        player.name = "player";
        player_x = (countCubes_x - 1.0f) / 2.0f;
        player_z = (countCubes_z - 1.0f) / 2.0f;
        player_y = countCubes_y + player_distance;
        player.transform.position = new Vector3(player_x, player_y, player_z);
        player.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        if (!player.GetComponent<Rigidbody>())
        {
            player.AddComponent<Rigidbody>();
            player.GetComponent<Rigidbody>().drag = 1.5f;
            player.GetComponent<Rigidbody>().angularDrag = 1.5f;
        }
        player.GetComponent<Rigidbody>().isKinematic = false;
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    // Use this for initialization
    void Start()
    {
        game_score = 0;
        end_of_game_text.enabled = false;
        exit_info.enabled = false;
        animation_once = true;
        floor.transform.position = new Vector3(floor.transform.position.x, 0, floor.transform.position.z);
        y = this.transform.localScale.y / 2;
        Create_layer(y,true);
        Create_layer(cube.transform.localScale.y*3+ this.transform.localScale.y / 2,true);
        Create_layer(-2.5f);
        Create_player();
        Camera_connect.point_x = player.transform.position.x;
        Camera_connect.point_z = player.transform.position.z;
        pause_status = true;
    }

    void PlayAnimation()
    {
        if (animation_once)
        {
            end_of_game_text.GetComponent<Animation>().Play();
            animation_once=false;
        }
    }

    public void PauseOn()
    {
        pause_status = true;
        pause_info.enabled = true;
        pause_button.image.enabled = false;
    }

    private void LateUpdate()
    {
        //ПАУЗА
        if (pause_status)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
            pause_button.image.enabled = true;
            pause_info.enabled = false;
        }
        //------------------------------------//
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (first_back_click)
            {
                if (time_remain_for_exit > 0)
                    Application.Quit();
            }
            else
            {
                first_back_click = true;
                exit_info.enabled = true;
                time_remain_for_exit = 3f;
            }
        }
        if (time_remain_for_exit<=0)
        {
            exit_info.enabled = false;
            first_back_click = false;
        }
        if (animation_once)
        {
            end_of_game_text.transform.position = new Vector3(end_of_game_text.transform.position.x, -100, end_of_game_text.transform.position.z);
            animation_once = false;
        }
        if (end_of_game_text.enabled)
        {
            if (end_of_game_text.transform.position.y < Screen.height / 2.0f)
            {
                end_of_game_text.transform.position = new Vector3(end_of_game_text.transform.position.x, end_of_game_text.transform.position.y + speed_of_animation, end_of_game_text.transform.position.z);
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (pause_status == true)
                pause_status = false;
        }
    }

    private void FixedUpdate()
    {
       
    }


    // Update is called once per frame
    void Update()
    {
        if (player.transform.position.x <= -1f || player.transform.position.x >= 3f || player.transform.position.z <= -1f || player.transform.position.z >= 3f)
        {
            try
            {
                for (int i = 0; i < GameObject.FindGameObjectsWithTag("Figure").Length; i++)
                {
                    GameObject.FindGameObjectsWithTag("Figure")[i].GetComponent<Rigidbody>().isKinematic = true;
                    //GameObject.FindGameObjectsWithTag("Figure")[i].tag = "Untagged";
                }
            }
            catch { }
            player.GetComponent<Rigidbody>().isKinematic = true;
            end_of_game_text.enabled = true;
            //PlayAnimation();
            Camera_connect.EndOfGame = true;
        }
        else
        {
            main_camera.transform.localPosition = new Vector3(main_camera.transform.localPosition.x, 3f + player.transform.localPosition.y, main_camera.transform.localPosition.z);
            if (player.transform.position.y - floor.transform.localPosition.y <= 4f)
            {
                
                Create_layer(last_y);
                floor.transform.localPosition = new Vector3(floor.transform.localPosition.x, floor.transform.localPosition.y - 3f, floor.transform.localPosition.z);
            }
        }
        game_score_text.text =  game_score.ToString();
        if (Camera_connect.rdy_to_start_new_game)
        {
            //НАЧАЛО НОВОЙ ИГРЫ
            try
            {
                for (int i = 0; i < GameObject.FindGameObjectsWithTag("Figure").Length; i++)
                {
                    Destroy(GameObject.FindGameObjectsWithTag("Figure")[i]);
                }
            }
            catch
            { }
            Camera_connect.EndOfGame = false;
            Camera_connect.rdy_to_start_new_game = false;
            Start();
        }
        if (time_remain_for_exit>0)
        {
            time_remain_for_exit -= Time.deltaTime;
        }
    }
}