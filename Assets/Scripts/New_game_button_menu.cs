﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class New_game_button_menu : MonoBehaviour {

    public void But_new_game_click()
    {
        SceneManager.LoadScene("Main");
    }
}
