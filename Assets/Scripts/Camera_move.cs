﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_move : MonoBehaviour {
    Quaternion originRotation;
    float angleHorizontal;
    float angleVertical;
    public float mouseSens;

	// Use this for initialization
	void Start () {
        originRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetMouseButton(1))
        {
            angleHorizontal += Input.GetAxis("Mouse X") * mouseSens;
            angleVertical += Input.GetAxis("Mouse Y") * mouseSens;
            angleVertical = Mathf.Clamp(angleVertical, -90, 90);
            Quaternion rotationY = Quaternion.AngleAxis(angleHorizontal, Vector3.up);
            Quaternion rotationX = Quaternion.AngleAxis(-angleVertical, Vector3.right);
            transform.rotation = originRotation * rotationX * rotationY;
        }
    }
}
