﻿using UnityEngine;
using System.Collections;

public class Camera_connect : MonoBehaviour
{

    public Transform target;
    public Vector3 offset;
    public float sensitivity = 31; // чувствительность мышки
    public float limit = 80; // ограничение вращения по Y
    public float zoom = 0.25f; // чувствительность при увеличении, колесиком мышки
    public float zoomMax = 2; // макс. увеличение
    public float zoomMin = 1; // мин. увеличение
    private float X, Y;
    static public float point_x,point_z;
    static public bool EndOfGame, rdy_to_start_new_game;
    void Start()
    {
        rdy_to_start_new_game = false;
        EndOfGame = false;
        limit = Mathf.Abs(limit);
        if (limit > 90)
            limit = 90;
        offset = new Vector3(offset.x, offset.y, -Mathf.Abs(zoomMax)/1.3f);
        transform.position = target.position + offset;
        //Debug.Log(GetComponent<Renderer>().material.GetColor("_Color1"));
        GetComponent<Skybox>().material.SetColor("_Color1", Color.yellow);
        GetComponent<Skybox>().material.SetColor("_Color2", Color.red);
        ////////////////////////////////////////////////////////////////////Градиентный фон//////////////////////////////////////////////////////////////////////////////
        {
            GetComponent<Skybox>().material.SetColor("_Color1", new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
            GetComponent<Skybox>().material.SetColor("_Color2", new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    void Update()
    {
            if (!EndOfGame)
            {
            if (!Game_Main_script.pause_status)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                    offset.z += zoom;
                else
                      if (Input.GetAxis("Mouse ScrollWheel") < 0)
                    offset.z -= zoom;
                offset.z = Mathf.Clamp(offset.z, -Mathf.Abs(zoomMax), -Mathf.Abs(zoomMin));
                if (Input.GetMouseButton(1))
                {
                    X = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivity;
                    Y = -transform.localEulerAngles.x;
                    // Y = Mathf.Clamp(Y, -limit, limit);
                    transform.localEulerAngles = new Vector3(-Y, X, 0);
                }
            }
                transform.position = transform.localRotation * offset + new Vector3(point_x, target.position.y, point_z);// + Vector3(point.x,target,point.z);
            }
    }
}