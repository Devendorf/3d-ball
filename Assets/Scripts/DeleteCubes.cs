﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeleteCubes : MonoBehaviour {

    private GameObject floor;

	// Use this for initialization
	void Start () {
        floor = GameObject.FindGameObjectWithTag("Floor");
    }

    // Update is called once per frame
    void Update()
    {
        if (!Game_Main_script.pause_status)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!Camera_connect.EndOfGame)
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    Physics.Raycast(ray, out hit, 300);
                    {
                        try
                        {
                            if (hit.collider.gameObject.tag != "Player")
                            {
                                if (hit.collider.gameObject.transform.parent.gameObject != null)
                                    Destroy(hit.collider.gameObject.transform.parent.gameObject);
                                else
                                    Destroy(hit.collider.gameObject);
                                Game_Main_script.game_score++;
                            }
                        }
                        catch
                        {
                        }
                    }
                }
                else
                {
                    Camera_connect.rdy_to_start_new_game = true;
                }
            }
            if (this.gameObject.transform.position.y < floor.transform.localPosition.y - 5f)
            {
                try
                {
                    if (this.gameObject.transform.parent.gameObject != null)
                        Destroy(this.gameObject.transform.parent.gameObject);
                }
                catch
                {
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
